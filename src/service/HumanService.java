package service;

import model.Human;

public class HumanService {
    private Human[] humans = new Human[100];
    private int index = 0;

    public boolean addHuman(Human human){
        if (!hasHuman(human.getName())){
            humans[index++] = human;
            System.out.println("Human added successfully.");
            return true;
        }
        System.out.println("Human already exists?");
        return false;
    }
    public boolean hasHuman(String name){
        for (Human human : humans){
            if (human!=null && human.getName().equals(name)){
                return true;
            }
        }
        return false;
    }

    public Human[] getHumans(){
        return humans;
    }
}
