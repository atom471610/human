import model.Human;
import service.HumanService;

import java.util.Scanner;

//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class Main {
    static Scanner scannerInt = new Scanner(System.in);
    static Scanner scannerStr = new Scanner(System.in);
    public static void main(String[] args) {
        HumanService humanService = new HumanService();
        int step = 10;
        while (step!=0){
            System.out.println("1.Add human. 2.Show human list. 3.Delete human. 4.Exit?");
            step = scannerInt.nextInt();
            switch (step){
                case 1 ->{
                    Human human = new Human();
                    System.out.println("Enter name : ");
                    human.setName(scannerStr.nextLine());
                    System.out.println("Enter age : ");
                    human.setAge(scannerStr.nextLine());
                    System.out.println("Enter job : ");
                    human.setJob(scannerStr.nextLine());
                    humanService.addHuman(human);
                }
                case 2 -> {
                    Human[] humans = humanService.getHumans();
                    getHuman(humans);
                }
            }
        }
    }

    public static void getHuman(Human[] humans){
        for (int i=0;i< humans.length;i++){
            if (humans[i]!=null){
                System.out.println("Human name:" + humans[i].getName() + " job:" + humans[i].getJob()
                        + " age: " + humans[i].getAge() );
            }
        }
    }
}